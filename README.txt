Important:
- this code cannot be relied on. it is not tested for all use cases.
- there is no claim that this code works in all use cases.
- you should test what you create from this code, for your use case.
- i have tested it for my use cases only, and for that it works.

This project is for a Julia script and its modules:
- select text files to combine together in a new output file.
- verify that each source line is exactly present in output file.
- display text report of outcome of the process.
- optionally append to a receiver file.
- optionally sort lines of output file.

Whats special about this code?
- automatic verification of output files.
- it includes verification that all source lines are present exactly in the output file.
- output report includes stats to show verification success.
- upon error, the process is aborted and output files are deleted.

Script was written to use in Linux.
- minor refactoring to use with windows.
  - in windows use Tk instead of Gtk.

Privacy statement
- this code does not collect or transmit any data.

Freedom statement
- use without crediting, obligation or payment.

==== end ====
