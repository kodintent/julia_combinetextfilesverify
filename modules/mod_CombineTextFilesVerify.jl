module CombineTextFilesVerify
#= =#

include("./mod_Functions.jl")
using .Functions
include("./mod_FileLineVerifyBoolStr.jl")
using .FileLineVerify

using Dates

export
    get_file_lines_or_nothing,
    start_combine_and_verify

function start_combine_and_verify(array_files_add::AbstractArray, file_receiver::AbstractString, bool_sort::Bool, bool_cleanup_append::Bool, path_trash::AbstractString)
    #println("Module - combine start")
    have_receiver_file = file_receiver == "" ? false : true

    # GET LINES FROM FILES

    array_lines_receiver = []
    if have_receiver_file # do this first in case error with that file
        array_lines_receiver = get_file_lines_or_nothing(file_receiver, bool_sort)
        if array_lines_receiver === nothing
            return " Error. Receiver file is not readable. Aborted."
        end
    end

    num_files_add = length(array_files_add)
    array_lines_add = get_lines_from_files(array_files_add, bool_sort)
    if array_lines_add === nothing
        return " Error. A source file is not readable. Aborted."
    end
    num_lines_add = length(array_lines_add)
    #println("Module - lines collected in arrays")

    # SORT LINES IF REQUIRED - if bool sort, then all blank lines excluded
    if bool_sort && num_files_add > 1  # 
        sort!(array_lines_add)
    end
    #println("Module - lines sorted if required")

    #PREPARE ARRAYS FOR SAVED FILES

    array_lines_all = []
    if have_receiver_file
        append!(array_lines_all, array_lines_receiver)
    end
    append!(array_lines_all, array_lines_add)
    if bool_sort
        sort!(array_lines_all)
    end

    # SAVE COMBINED LINES TO A NEW FILE

    combine_result = ""
    combine_extra_prefix = "Merged: "
    # maybe need to save and validate separately so as to have the filename for removing if another verification doesnt work.
    if have_receiver_file
        combine_extra_prefix = "Appended: "
        num_lines_receiver = length(array_lines_receiver)
        num_lines_total = num_lines_receiver + num_lines_add
        num_lines_collected = length(array_lines_all)
        filename_extra = string("Combined: receiver $num_lines_receiver lines & $num_files_add files $num_lines_add lines total $num_lines_total lines")
        crosscheck_string = string("Check: collected $num_lines_collected lines")
        combine_result = string(combine_result, " ", filename_extra, " ", crosscheck_string)
        (is_saved_validated, result_save_validate) = save_and_validate_file(array_lines_all, file_receiver, filename_extra, true, bool_sort)
        combine_result = string(combine_result, result_save_validate)
        if !is_saved_validated # created file already deleted
            return combine_result # without proceeding to saving add files
        end
    end
    # if have_receiver_file then it must validate to reach here
    filename_extra = string(combine_extra_prefix, "$num_files_add files $num_lines_add lines")
    combine_result = string(combine_result, " ", filename_extra)
    (is_saved_validated, result_save_validate) = save_and_validate_file(array_lines_add, array_files_add[1], filename_extra, false, bool_sort)
    combine_result = string(combine_result, result_save_validate)

    if is_saved_validated && bool_cleanup_append
        for filepath_add in array_files_add
            Functions.trash_file_truefalse(filepath_add, false, bool_cleanup_append, path_trash)
        end
        combine_result = string(combine_result, " Append files merged to new file and sources moved to Trash.")
    end

    #println("verify_lines_result retrieved: $verify_lines_result")
    return combine_result
end

##################### SUB FUNCTIONS ###########################

function save_and_validate_file(array_lines_original::AbstractArray, filepath_name_source::AbstractString, filename_extra::AbstractString, bool_append_filename::Bool, bool_sort::Bool)
    save_validate_result = ""
    (is_saved, filepath_saved) = save_file(array_lines_original, filepath_name_source, filename_extra, bool_append_filename)
    if is_saved
        save_validate_result = string("Saved ", Functions.get_path_child(filepath_saved))
        (is_validated, verify_lines_result) = validate_file(array_lines_original, filepath_saved, bool_sort)
        save_validate_result = verify_lines_result
        if !is_validated
            rm(filepath_saved)
            save_validate_result = string(save_validate_result, " Error. Saved file not validated thus deleted. Aborted.")
        end
    else
        save_validate_result = " Error. Combined file could not be created. Aborted."
    end
    return (is_validated, save_validate_result)
end

function save_file(array_lines_original::AbstractArray, filepath_name_source::AbstractString, filename_extra::AbstractString, bool_append_filename::Bool)
    outputname_type = bool_append_filename ? "_Appended_" : "_Combined_"
    filepath_combined_base = make_output_filepathname_base(filepath_name_source, outputname_type, bool_append_filename)
    filepath_combine_output = string(filepath_combined_base, "_", filename_extra, ".txt")
    string_lines = join(array_lines_original, "\n")
    is_saved = Functions.trysave_filestring_truefalse(filepath_combine_output, string_lines)
    return (is_saved, filepath_combine_output)
end

function validate_file(array_lines_original::AbstractArray, filepath_saved::AbstractString, bool_sort::Bool)
    array_lines_saved_file = get_file_lines_or_nothing(filepath_saved, bool_sort)
    if array_lines_saved_file === nothing
        return (false, " Error: Could not retrieve saved file lines for verifying. Check file location access. Aborted.")
    end

    (is_validated, verify_lines_result) = FileLineVerify.check_lines_arein_linearray_exact(array_lines_original, array_lines_saved_file)
    return (is_validated, verify_lines_result)
end

function get_file_lines_or_nothing(filepath::AbstractString, bool_sort::Bool)
    exclude_empty_lines = bool_sort ? true : false
    array_lines = Functions.get_arraylines_from_file_errnothing(filepath, true, bool_sort, exclude_empty_lines)
    if array_lines === nothing
        return nothing
    end
    return array_lines
end


end #module
