module FileLineVerify

# this module verifies the presence of lines in a reference file.
# it breaks up the lines array into 3 levels according to value passed in.
# generic form of the code made for comparing journal entry lines quickly.

export
check_lines_arein_linearray,
check_lines_arein_linearray_exact,
check_lines_arein_linearray_exact_appendfilename

include("./mod_Functions.jl")
using .Functions

function check_lines_arein_linearray_exact(array_lines_primary::AbstractArray, array_lines_newer::AbstractArray)
    check_lines_arein_linearray(array_lines_primary, array_lines_newer, "", false, false)
end
function check_lines_arein_linearray_exact_appendfilename(array_lines_primary::AbstractArray, array_lines_newer::AbstractArray, selected_file_old::AbstractString)
    check_lines_arein_linearray(array_lines_primary, array_lines_newer, selected_file_old, false, false)
end

function check_lines_arein_linearray(array_lines_primary::AbstractArray, array_lines_newer::AbstractArray, selected_file_old::AbstractString, rate_if_inexact::Bool, bool_compare_month::Bool)
    println("      FLV - check_lines_arein_linearray_m start")
    bool_success = true
    primary = "source"
    secondary = "merged"
    if rate_if_inexact
        primary = "older"
        secondary = "newer"
    end

    result_string = ""
    array_lines_rated = []
    array_lines_error = []

    count_lines_primary_found = 0
    count_lines_tested = 0
    count_lines_exact = 0
    count_lines_test_err = 0
    count_lines_blank = 0
    count_lines_tooshort = 0
    currentbatch_y = ""
    currentbatch_m = ""
    currentbatch_d = ""
    slice_newer_current_y = []
    slice_newer_current_m = []
    slice_newer_current_d = []
    length_array_lines_primary = length(array_lines_primary)
    length_array_lines_newer = length(array_lines_newer)
    result_string = string("$result_string Verify: $length_array_lines_primary $primary lines, $length_array_lines_newer $secondary lines.")

    #line_report_frequency = Int(floor(Int, length_array_lines_primary / 500))

    println("      FLV - File line verification line looping begins...")

    bool_shortlines_notyet_filtered = true
    array_newer_shortlines = []
    for line_primary in array_lines_primary
        
        count_lines_primary_found += 1

        #println("Line Loops: $count_lines_found lines.")
        #print("\e[2K\e[1G    Processed $count_lines_found lines...")
        #if count_lines_found % line_report_frequency == 0
        #if count_lines_found % 500 == 0
        #    print("\e[2K\e[1G      FLV - Processed $count_lines_found of $length_array_lines_primary lines...")
        #end
        
        if strip(line_primary) == ""
            count_lines_blank += 1
            continue 
        end # blank line, ignore

        count_lines_tested += 1

        if length(line_primary) < 10
            # should still search all lines for this line, to verify it.
            # the huge journal file should never have such a short line.
            count_lines_tooshort += 1
            if bool_shortlines_notyet_filtered
                array_newer_shortlines = filter(x -> length(x) < 10, array_lines_newer)
                bool_shortlines_notyet_filtered = false
            end
            if line_is_exact_match(line_primary, array_newer_shortlines)
                count_lines_exact += 1
            elseif rate_if_inexact
                line_rated = rate_older_line(array_newer_shortlines, line_primary)
                push!(array_lines_rated, line_rated)
            else
                return (false, string("$result_string An older file line was not perfectly matched when exact is required. End comparison."))
            end 
            continue
        end # line is too short to be sliced, but check separately

        # test for the special quotes that seem to break the verify code. added 20211203 but removed bc not working
        #if occursin("""“""", line_primary) || occursin("""”""", line_primary) || occursin("""’""", line_primary)
        #    println("found line with special quotes")
        #    chars2replace = Dict('“' => '"', '”' => '"', "’" => "'")
        #    line_primary_edited = replaceCharsInStringByJoin(line_primary, chars2replace)
        #    line_primary = line_primary_edited
        #    println(line_primary)
        #end

        # encase slicing and testing in try catch because of special chars
        try
            # UPDATE NEWER LINES SLICES

            current_line_y = line_primary[begin:4]
            current_line_m = line_primary[begin:7]
            current_line_d = line_primary[begin:10]
            if current_line_y != currentbatch_y
                currentbatch_y = current_line_y
                slice_newer_current_y = filter(x -> startswith(x, currentbatch_y), array_lines_newer)
            end
            if current_line_m != currentbatch_m
                currentbatch_m = current_line_m;
                slice_newer_current_m = filter(x -> startswith(x, currentbatch_m), slice_newer_current_y)
            end
            if current_line_d != currentbatch_d
                currentbatch_d = current_line_d;
                slice_newer_current_d = filter(x -> startswith(x, currentbatch_d), slice_newer_current_m)
            end
            
            # CHECK IF LINE IS PRESENT

            if line_is_exact_match(line_primary, slice_newer_current_d)
                count_lines_exact += 1
            elseif rate_if_inexact
                if bool_compare_month
                    line_rated = rate_older_line(slice_newer_current_m, line_primary)
                else
                    line_rated = rate_older_line(slice_newer_current_d, line_primary)
                end
                push!(array_lines_rated, line_rated)
            else
                return (false, string("$result_string An older file line was not perfectly matched when exact is required. End comparison."))
            end 
        catch
            count_lines_test_err += 1
            count_lines_tested -= 1
            push!(array_lines_error, line_primary)
            result_string = string("$result_string \nLineTestFail after $count_lines_tested: \"$line_primary.\"")
            println("Line test fail: $line_primary")

            if !rate_if_inexact # added 20220813 when error didnt abort
                return (false, result_string) # return on error if exact req
            end
        end
    end

    # LINE LOOPS COMPLETED
    println("      FLV - finished line loops.")
    num_lines_not_tested = count_lines_test_err + count_lines_blank
    num_lines_added_up = count_lines_test_err + count_lines_blank + count_lines_tested
    result_string = string("$result_string $count_lines_primary_found $primary lines processed. Exactly matched $count_lines_exact of $count_lines_tested tested. $num_lines_not_tested lines not tested including $count_lines_test_err test errors, $count_lines_blank blank. Check: Tested+Error+Blank: $count_lines_tested+$count_lines_test_err+$count_lines_blank=$num_lines_added_up.")

    # saving if necessary and reporting

    length_array_lines_rated = length(array_lines_rated)
    if length_array_lines_rated > 0
        #println("length_array_lines_rated = $length_array_lines_rated - save expected")
        #save it
        result_string = string("$result_string Rated $length_array_lines_rated lines")
        #println("result_string = $result_string")
        #save the rated lines to a specially named file.
        filepath_rated_lines = string(selected_file_old[begin:end-4], "_rated_lines.txt")
        #println("filepath_rated_lines = $filepath_rated_lines")
        # maybe include some intro lines with file info.
        string_header = "These lines from the files to compare, were not exactly matched to lines in the reference file.\r\n\r\n"
        sort!(array_lines_rated)
        string_lines_rated = join(array_lines_rated, "\n")
        string_file_content = string(string_header, string_lines_rated)
        saved = Functions.trysave_filestring_truefalse(filepath_rated_lines, string_file_content)
        if saved
            filename = Functions.get_path_child(filepath_rated_lines)
            result_string = string("$result_string - saved to \"$filename\" in same folder as older file.")
            #println("    Rated lines saved to folder with reference file.")
        else
            bool_success = false
            result_string = string("$result_string Rated lines collected but save to a file failed. Advice: repeat the operation.")
            #rm(filepath_rated_lines, force=true)
            #println("    Rated lines created but not saved to a file.")
            #verification_successful = false
        end
    else
        if rate_if_inexact
            result_string = string("$result_string Lines all exact, no rated lines file saved.")
            println("      FLV - Lines all exact, no rated lines saved.")
        end
    end

    # if lines skipped because <10 chars or error when comparing
    length_array_lines_skipped = length(array_lines_error)
    if length_array_lines_skipped > 0 && selected_file_old != ""
        filepath_skipped_lines = string(selected_file_old[begin:end-4], "_lineverify_skipped_lines.txt")
        string_header = "These lines from the files to compare, were skipped as they were too short, empty or failed verification somehow.\r\n\r\n"
        string_lines_skipped = join(array_lines_error, "\n")
        string_file_skipped = string(string_header, string_lines_skipped)
        saved_skipped = Functions.trysave_filestring_truefalse(filepath_skipped_lines, string_file_skipped)
        if saved_skipped
            filename_skipped = Functions.get_path_child(filepath_skipped_lines)
            result_string = string("$result_string $length_array_lines_skipped skipped lines saved to \"$filename_skipped\" in same folder as older file.")
        else
            bool_success = false
            result_string = string("$result_string $length_array_lines_skipped skipped lines collected but save to a file failed. Advice: repeat the operation.")
            #rm(filepath_skipped_lines, force=true)
        end
    end

    println("      FLV - final result_string = $result_string")
    return (bool_success, result_string)
end

function get_file_lines_or_nothing(filepath::AbstractString)
    array_lines = Functions.get_arraylines_from_file_errnothing(filepath, true, true, true)
    if array_lines === nothing return nothing end
    #if array_lines == [] return nothing end  # removed, if file is empty so what?
    return array_lines
end

function line_is_exact_match(
    line_primary::AbstractString, slice_newer_current_d::AbstractArray)
    for line_newer in slice_newer_current_d
        if strip(line_newer) == strip(line_primary) return true end
    end
    return false
end

#function replaceCharsInStringByJoin(string::AbstractString, chars2replace::Dict)
#    return join([chars2replace[c] for c in string])
#end

function rate_older_line(
    slice_newer_current_d::AbstractArray, line_primary_to_rate::AbstractString)
    match_rating_max = 0

    for newer_line in slice_newer_current_d
        total_source_word_count = 0
        total_source_word_match_count = 0
        for word_rate in split(line_primary_to_rate, " ") # check if it is in the reference line
            total_source_word_count += 1
            if occursin(word_rate, newer_line) total_source_word_match_count += 1 end
        end
        if total_source_word_match_count > 0 # match count is > 0, calculate the rating and compare
            line_match_rating = floor(100 * total_source_word_match_count / total_source_word_count)
            if match_rating_max < line_match_rating match_rating_max = line_match_rating end
        end
    end

    #get into from float
    match_rating_max_int = Int(floor(Int, match_rating_max))


    if match_rating_max_int > 99 match_rating_max_int = 99 end
    if match_rating_max_int < 10
        line_rated = string("0", match_rating_max_int, "% ", line_primary_to_rate)
    else
        line_rated = string(match_rating_max_int, "% ", line_primary_to_rate)
    end
    return line_rated
end




end # module
