module Functions

using Dates

export
trash_file_truefalse,
trysave_filestring_truefalse,
copy_optionalforce_preserve_metadata,
get_arraylines_from_file_errnothing,
tryget_filelines_errnothing,
get_path_parent_child_tuple,
get_path_child,
get_path_parent,
get_pathnoextension_extension_tuple_from_filename,
get_pathnoextension,
get_new_filepathbase_withdatestimestampiso,
get_lines_from_files,
make_output_filepathname_base,
get_file_lines_or_nothing

function get_lines_from_files(array_filepaths, bool_sort)
    array_lines_all = []
    for filepath in array_filepaths
        array_lines = get_file_lines_or_nothing(filepath, bool_sort)
        if array_lines === nothing
            return nothing
        end
        append!(array_lines_all, array_lines)
    end
    return array_lines_all
end

function make_output_filepathname_base(filepath::AbstractString, prefix::AbstractString, bool_append_filename::Bool)
    if bool_append_filename
        parent, child = Functions.get_path_parent_child_tuple(filepath)
        filename_noext = Functions.get_pathnoextension(child)
        prefix = string(filename_noext, prefix)
    end
    return get_new_filepathbase_withdatestimestampiso(filepath, prefix)
end

function get_file_lines_or_nothing(filepath::AbstractString, bool_sort::Bool)
    exclude_empty_lines = bool_sort ? true : false
    array_lines = Functions.get_arraylines_from_file_errnothing(filepath, true, bool_sort, exclude_empty_lines)
    if array_lines === nothing
        return nothing
    end
    return array_lines
end

# SEND FILE TO TRASH

function trash_file_truefalse(path_file::AbstractString, bool_force::Bool, bool_move::Bool, path_trash::AbstractString)
    path_trash_file = joinpath(path_trash, "files")
    path_trash_info = joinpath(path_trash, "info")

    parent, child = splitdir(path_file)

    path_file_uri = escapepath(path_file) # using URIs.jl
    deletiondate = Dates.format(now(), "yyyy-mm-ddTHH:MM:SS")
    string_info_file = string("[Trash Info]\nPath=", path_file_uri, "\nDeletionDate=", deletiondate)
    path_file_info = string(path_trash_info, "/", child, ".trashinfo")
    saved_file_info = trysave_filestring_truefalse(path_file_info, string_info_file)

    # move with preserving file metadata
    path_file_trash = joinpath(path_trash_file, child)
    moved2trash = copy_optionalforce_preserve_metadata(path_file::AbstractString, path_file_trash::AbstractString, bool_force::Bool)

    if !saved_file_info || !moved2trash
        if isfile(path_trash_file) rm(path_trash_file) end
        if isfile(path_trash_info) rm(path_trash_info) end
        return false
    end
    if bool_move rm(path_file) end
    return true

end
    
# SAVE

function trysave_filestring_truefalse(filepath_output::AbstractString, content_string::AbstractString)
    try
        open(filepath_output, "w") do io
            write(io, content_string)
        end
    catch
        return false
    end
    return true
end

# MOVE - use CLI copy version, to preserve file metadata

function copy_optionalforce_preserve_metadata(path_source::AbstractString, path_dest::AbstractString, bool_force::Bool)
    try
        if bool_force
            copy_result = run(`cp -a -f $path_source $path_dest`)
        else
            copy_result = run(`cp -a -n $path_source $path_dest`)
        end
    catch
        return false
    end
    return true
end

function get_arraylines_from_file_errnothing(filepath::AbstractString, strip_breaks::Bool, sortlines::Bool, exclude_empty_lines::Bool)

    array_lines_file = tryget_filelines_errnothing(filepath)
    if array_lines_file === nothing return nothing end
    if length(array_lines_file) == 0
        println("    get_arraylines_from_file_errnothing - filepath returned 0 lines.")
        return array_lines_file
    end
    # filter lines if bools require it.
    if strip_breaks || exclude_empty_lines
        array_lines_edited = []
        for line in array_lines_file
            line_keep = line
            line_stripped = strip(line, [' ', '\r', '\n'])
            if strip_breaks line_keep = line_stripped end
            if exclude_empty_lines && line_stripped == "" continue end
            push!(array_lines_edited, line_keep)
        end
        if sortlines sort!(array_lines_edited) end
        return array_lines_edited
    end

    # else return unfiltered lines
    if sortlines sort!(array_lines_file) end
    return array_lines_file
end

function tryget_filelines_errnothing(filepath::AbstractString)
    try
        open(filepath, "r") do io
            return readlines(io)
        end
    catch
        return nothing
    end
end

function get_path_parent_child_tuple(path::AbstractString)
    return splitdir(path)
end
function get_path_parent(path::AbstractString)
    parent, child = splitdir(path)
    return parent
end
function get_path_child(path::AbstractString)
    parent, child = splitdir(path)
    return child
end

function get_pathnoextension_extension_tuple(path::AbstractString)
    return get_pathnoextension_extension_tuple_from_filename(path)
end

function get_pathnoextension_extension_tuple_from_filename(path::AbstractString)
    indexExtDotRange = findlast(".", path)
    if indexExtDotRange === nothing return (path, "") end
    indexExtDot = indexExtDotRange[begin]
    if path[indexExtDot-1] == "/" return (path, "") end # dot start filename
    pathnoext = path[begin:indexExtDot-1]
    ext = path[indexExtDot:end]
    return (pathnoext, ext)
end
function get_pathnoextension(path::AbstractString)
    pathnoextension, extension = get_pathnoextension_extension_tuple(path)
    return pathnoextension
end

function get_new_filepathbase_withdatestimestampiso(path::AbstractString, prefix::AbstractString)
    datetimenowlocal = Dates.format(now(), "yyyymmddTHHMMSS")
    parentpath = get_path_parent(path)
    return string(parentpath, "/", prefix, datetimenowlocal)
end


end #module
