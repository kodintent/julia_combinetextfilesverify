#=
Combine text files, and if a source file is selected, append to that.
Code is for linux, so if using on windows, need to modify some parts.
cleanup sends append source files to linux trash folder.
currently this code cannot process backticks ``
=#

include("modules/mod_CombineTextFilesVerify.jl") #relative path for paired module
using .CombineTextFilesVerify

using Gtk

# DEFAULT CONFIG VARIABLES
bool_sort = false
bool_ready = true
bool_cleanup = false
#username = ENV["USER"]
username = splitdir(homedir())[end] # works on linux and windows?
path_trash = "/home/$username/.local/share/Trash/files"
text_label_info = "Creates a new file. If receiver set, output saved there."

function get_path_child(path::AbstractString)
    parent, child = splitdir(path)
    return child
end
function get_filepath_any_bydialog(title::AbstractString="", selectN::Bool=false)
    return open_dialog(title, select_multiple=selectN)
end

# CREATE THE GUI
textbox_info = GtkLabel(text_label_info)
set_gtk_property!(textbox_info, :xalign, 0.5)
GAccessor.line_wrap(textbox_info, true)

button_setfile_receive = GtkButton("Set Receive File")
text_file_receive = GtkEntry()
set_gtk_property!(text_file_receive, :xalign, 0.1)
set_gtk_property!(text_file_receive, :editable, false)

button_setfiles_append = GtkButton("Set Source Files")
text_files_append = GtkEntry()
set_gtk_property!(text_files_append, :xalign, 0.1)
set_gtk_property!(text_files_append, :editable, false)

button_start = GtkButton("Start")

checkbutton_sort = GtkCheckButton("Sort result")
set_gtk_property!(checkbutton_sort, :active, bool_sort)

checkbutton_cleanup = GtkCheckButton("Cleanup added source files")
set_gtk_property!(checkbutton_cleanup, :active, bool_cleanup)

textbox_output = GtkLabel("")
set_gtk_property!(textbox_output, :xalign, 0.0)
GAccessor.selectable(textbox_output, true)
GAccessor.line_wrap(textbox_output, true)

# create the grid and arrange widgets in it
grid = GtkGrid()
set_gtk_property!(grid, :column_homogeneous, true)
grid[1:6, 1] = textbox_info # span and set 6 columns
grid[1, 2] = button_setfile_receive
grid[2:6, 2] = text_file_receive
grid[1, 3] = button_setfiles_append
grid[2:6, 3] = text_files_append
grid[1, 4] = button_start
grid[3, 4] = checkbutton_sort
grid[4:6, 4] = checkbutton_cleanup
grid[1:6, 5] = textbox_output

#create the window and add the grid to it.
window = GtkWindow("Combine Files and Verify Result", 700, 300)
GAccessor.resizable(window, true)
set_gtk_property!(window, :border_width, 5)
push!(window, grid)
showall(window)

# FUNCTIONS

file_receive = ""
array_files_append = []

button_setfile_receive_triggered = signal_connect(button_setfile_receive, "clicked") do widget
    global file_receive = get_filepath_any_bydialog("SELECT RECEIVER FILE...", false) # false for select 1 file only
    GAccessor.text(text_file_receive, get_path_child(file_receive))
end

button_setfiles_append_triggered = signal_connect(button_setfiles_append, "clicked") do widget
    global array_files_append = get_filepath_any_bydialog("SELECT SOURCE FILES...", true)
    length_array_files_append = length(array_files_append)
    if length_array_files_append == 1
        GAccessor.text(text_files_append, get_path_child(array_files_append[begin]))
    else
        GAccessor.text(text_files_append, string("$length_array_files_append files selected."))
    end
end

checkbutton_sort_triggered = signal_connect(checkbutton_sort, "toggled") do widget
    global bool_sort = get_gtk_property(checkbutton_sort, :active, Bool)
end

checkbutton_cleanup_triggered = signal_connect(checkbutton_cleanup, "toggled") do widget
    global bool_cleanup = get_gtk_property(checkbutton_cleanup, :active, Bool)
end

button_start_triggered = signal_connect(button_start, "clicked") do widget
    length_array_files_append = length(array_files_append)
    if bool_ready
        global bool_ready = false
        set_gtk_property!(textbox_output, :label, "")
        if array_files_append == Any[]
            set_gtk_property!(textbox_output, :label, " Append files not selected. Aborted.")
        elseif file_receive == "" && length_array_files_append < 2
            set_gtk_property!(textbox_output, :label, " Only one file selected. Aborted.")
        else
            result_string = CombineTextFilesVerify.start_combine_and_verify(array_files_append, file_receive, bool_sort, bool_cleanup, path_trash)
            # output and cleanup
            set_gtk_property!(textbox_output, :label, result_string)
        end
        bool_ready = true
    end
    #println("check. array_files_append = $array_files_append")
end

# fake condition keeps window open unless closed.
if !isinteractive()
    c = Condition()
    signal_connect(window, :destroy) do widget
        notify(c)
    end
    wait(c)
end




